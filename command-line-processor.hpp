/*
 *  embed-binary: Generate code that represents a binary file as a
 *                constant vector
 *  Copyright (C) 2018 Tom Norton
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef EMBED_BINARY_COMMAND_LINE_PROCESSOR_HPP
#define EMBED_BINARY_COMMAND_LINE_PROCESSOR_HPP

#include <cstring>
#include <string>

template <typename... Args>
int processCommandLineArgument(int argc, const char** argv, int currentIndex, Args... args);

template <typename... Args>
int processCommandLineArgument(int argc, const char** argv, int currentIndex,
                               const char* longOption, const char* shortOption, bool* pSwitch, Args... args);

template <typename... Args>
int processCommandLineArgument(int argc, const char** argv, int currentIndex,
                               const char* longOption, const char* shortOption, std::string* pValue, Args... args);

template <typename... Args>
int processCommandLineArgument(int argc, const char** argv, int currentIndex,
                               const char* longOption, const char* shortOption, int* pValue, Args... args);

template <typename... Args>
int processCommandLineArgument(int argc, const char** argv, int currentIndex, Args... args) {
    return -1;
}

template <typename... Args>
int processCommandLineArgument(int argc, const char** argv, int currentIndex,
                               const char* longOption, const char* shortOption, bool* pSwitch, Args... args) {
    if (std::strcmp(argv[currentIndex], longOption) == 0 || std::strcmp(argv[currentIndex], shortOption) == 0) {
        *pSwitch = true;
        return currentIndex + 1;
    }
    return processCommandLineArgument(argc, argv, currentIndex, args...);
}

template <typename... Args>
int processCommandLineArgument(int argc, const char** argv, int currentIndex,
                               const char* longOption, const char* shortOption, std::string* pValue, Args... args) {
    if (std::strcmp(argv[currentIndex], longOption) == 0) {
        if (++currentIndex >= argc) {
            return -1;
        }
        *pValue = argv[currentIndex];
        return currentIndex + 1;
    }
    if (std::string currentArgument = argv[currentIndex]; currentArgument.find(shortOption) == 0UL) {
        if (currentArgument.length() == std::strlen(shortOption)) {
            return -1;
        }
        *pValue = currentArgument.substr(std::strlen(shortOption));
        return currentIndex + 1;
    }
    return processCommandLineArgument(argc, argv, currentIndex, args...);
}

template <typename... Args>
int processCommandLineArgument(int argc, const char** argv, int currentIndex,
                               const char* longOption, const char* shortOption, int* pValue, Args... args) {
    if (std::strcmp(argv[currentIndex], longOption) == 0) {
        if (++currentIndex >= argc) {
            return -1;
        }
        try {
            *pValue = std::stoi(argv[currentIndex]);
        } catch (...) {
            return -1;
        }
        return currentIndex + 1;
    }
    if (std::string currentArgument = argv[currentIndex]; currentArgument.find(shortOption) == 0UL) {
        if (currentArgument.length() == std::strlen(shortOption)) {
            return -1;
        }
        try {
            *pValue = std::stoi(currentArgument.substr(std::strlen(shortOption)));
        } catch (...) {
            return -1;
        }
        return currentIndex + 1;
    }
    return processCommandLineArgument(argc, argv, currentIndex, args...);
}

template <class ShowHelpScreenPredicate, class RunLogicPredicate, typename... Args>
int processCommandLine(ShowHelpScreenPredicate showHelpScreen, RunLogicPredicate runLogicPredicate,
                       int argc, const char** argv,
                       Args... args) {
    int currentIndex = 1;
    while (currentIndex < argc && currentIndex != -1) {
        if (std::strcmp(argv[currentIndex], "-h") == 0 || std::strcmp(argv[currentIndex], "--help") == 0) {
            showHelpScreen();
            return 0;
        }
        currentIndex = processCommandLineArgument(argc, argv, currentIndex, args...);
    }
    if (currentIndex == -1) {
        showHelpScreen();
        return 1;
    }
    return runLogicPredicate();
}

#endif //EMBED_BINARY_COMMAND_LINE_PROCESSOR_HPP
