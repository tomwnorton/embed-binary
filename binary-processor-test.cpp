/*
 *  embed-binary: Generate code that represents a binary file as a
 *                constant vector
 *  Copyright (C) 2018 Tom Norton
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <sstream>
#include <catch.hpp>

#include "binary-processor.hpp"

using namespace std;

class Action {
public:
    Action(const BinaryProcessor& binaryProcessor, istream& in, ostream& out)
            : m_binaryProcessor(binaryProcessor), m_in(in), m_out(out) {}
    void operator()() { m_binaryProcessor.processBinaryFile(m_in, m_out); }
private:
    const BinaryProcessor& m_binaryProcessor;
    istream& m_in;
    ostream& m_out;
};

TEST_CASE("It can generate the C++ code for a file") {
    BinaryProcessor binaryProcessor("MY_CONSTANT", 4U);
    istringstream in(ios::in | ios::binary);
    ostringstream out;
    Action act(binaryProcessor, in, out);

    SECTION("an empty file generates an empty vector") {
        act();
        CHECK(out.str() == "const std::vector<std::uint8_t> MY_CONSTANT = {};\n");
    }

    SECTION("a file with one byte generates a vector with one element") {
        in.str("A");
        act();
        CHECK(out.str() == "const std::vector<std::uint8_t> MY_CONSTANT = {\n    0x41\n};\n");
    }

    SECTION("a file with a single-hex-digit byte generates a vector with one two-digit element") {
        in.str("\x0A");
        act();
        CHECK(out.str() == "const std::vector<std::uint8_t> MY_CONSTANT = {\n    0x0A\n};\n");
    }

    SECTION("a file with a null byte generates a vector with a null element") {
        in.str(string("\0", 1));
        act();
        CHECK(out.str() == "const std::vector<std::uint8_t> MY_CONSTANT = {\n    0x00\n};\n");
    }

    SECTION("a file with two bytes generates a vector with two elements") {
        in.str("AO");
        act();
        CHECK(out.str() == "const std::vector<std::uint8_t> MY_CONSTANT = {\n    0x41, 0x4F\n};\n");
    }

    SECTION("a file with more than four bytes generates a vector spanning multiple lines") {
        in.str("ABCDEFGHIJKLMNOPQRSTUVWXYZ\n");
        act();
        auto const expected = "const std::vector<std::uint8_t> MY_CONSTANT = {\n"s
                        + "    0x41, 0x42, 0x43, 0x44,\n"s
                        + "    0x45, 0x46, 0x47, 0x48,\n"s
                        + "    0x49, 0x4A, 0x4B, 0x4C,\n"s
                        + "    0x4D, 0x4E, 0x4F, 0x50,\n"s
                        + "    0x51, 0x52, 0x53, 0x54,\n"s
                        + "    0x55, 0x56, 0x57, 0x58,\n"s
                        + "    0x59, 0x5A, 0x0A\n"s
                        + "};\n";
        CHECK(out.str() == expected);
    }

    SECTION("each character is treated as an unsigned byte") {
        in.str("\x96\x9E");
        act();
        auto const expected = "const std::vector<std::uint8_t> MY_CONSTANT = {\n"s
                              + "    0x96, 0x9E\n"s
                              + "};\n";
        CHECK(out.str() == expected);
    }
}