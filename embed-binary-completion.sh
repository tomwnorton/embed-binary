#!/usr/bin/env bash
#  embed-binary: Generate code that represents a binary file as a
#                constant vector
#  Copyright (C) 2018 Tom Norton
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
_embed_binary()
{
    local currentWord previousWord possibleOptions
    currentWord=${COMP_WORDS[COMP_CWORD]}
    previousWord=${COMP_WORDS[COMP_CWORD - 1]}
    if [[ ${previousWord} = --file-name ]] ; then
        COMPREPLY=( $(compgen -f -- ${currentWord}) )
        compopt -o nospace
    else
        possibleOptions="--help --constant-name --column-width --file-name"
        COMPREPLY=( $(compgen -W "${possibleOptions}" -- ${currentWord}) )
    fi
    return 0
}
complete -F _embed_binary embed-binary