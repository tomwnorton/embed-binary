# Embed Binary File as Vector

`embed-binary` converts a binary file into a C++ source snippet of a
constant vector.

## Usage
```
> embed-binary --constant-name EMPTY_ZIP --column-width 11 --file-name empty.zip
const std::vector<std::uint8_t> EMPTY_ZIP = {
    0x50, 0x4B, 0x05, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
```

You can also use short parameters:
```
embed-binary -cEMPTY_ZIP -w11 -fempty.zip
```