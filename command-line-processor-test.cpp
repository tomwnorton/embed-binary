/*
 *  embed-binary: Generate code that represents a binary file as a
 *                constant vector
 *  Copyright (C) 2018 Tom Norton
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <catch.hpp>
#include "command-line-processor.hpp"

using namespace std;

struct CommandLineModel {
    string foo;
    int bar = 0;
    bool baz = false;
};

bool helpScreenCalled;
bool logicCalled;

void showHelpScreen() {
    helpScreenCalled = true;
}

const int RUN_LOGIC_RESULT = 9;
int runLogic() {
    logicCalled = true;
    return RUN_LOGIC_RESULT;
}

TEST_CASE("A CommandLineProcessor") {
    CommandLineModel model;
    helpScreenCalled = false;
    logicCalled = false;

    SECTION("reports invalid argument as failure") {
        const char* argv[] = { "program-name", "-badarg" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 2, argv);
        CHECK(result == 1);
        CHECK(helpScreenCalled);
        CHECK_FALSE(logicCalled);
    }

    SECTION("shows help screen for valid long help option") {
        const char* argv[] = { "program-name", "--help" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 2, argv);
        CHECK(result == 0);
        CHECK(helpScreenCalled);
        CHECK_FALSE(logicCalled);
    }

    SECTION("shows help screen for valid short help option") {
        const char* argv[] = { "program-name", "-h" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 2, argv);
        CHECK(result == 0);
        CHECK(helpScreenCalled);
        CHECK_FALSE(logicCalled);
    }

    SECTION("sets flag for valid long switch") {
        const char* argv[] = { "program-name", "--baz" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 2, argv,
                                        "--baz", "-b", &model.baz);
        CHECK(result == RUN_LOGIC_RESULT);
        CHECK(model.baz);
        CHECK(logicCalled);
    }

    SECTION("sets flag for valid short switch") {
        const char* argv[] = { "program-name", "-b" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 2, argv,
                                        "--baz", "-b", &model.baz);
        CHECK(result == RUN_LOGIC_RESULT);
        CHECK(model.baz);
        CHECK(logicCalled);
    }

    SECTION("reports error when the second argument is invalid") {
        const char* argv[] = { "program-name", "-b", "--bad-arg" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 3, argv,
                                        "--baz", "-b", &model.baz);
        CHECK(result == 1);
        CHECK(helpScreenCalled);
        CHECK_FALSE(logicCalled);
    }

    SECTION("shows help screen when the second argument is long help option") {
        const char* argv[] = { "program-name", "-b", "--help" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 3, argv,
                                        "--baz", "-b", &model.baz);
        CHECK(result == 0);
        CHECK(helpScreenCalled);
        CHECK_FALSE(logicCalled);
    }

    SECTION("sets string value for valid string switch using a long switch") {
        const char* argv[] = { "program-name", "--foo", "the value" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 3, argv,
                                        "--foo", "-f", &model.foo);
        CHECK(result == RUN_LOGIC_RESULT);
        CHECK(logicCalled);
        CHECK(model.foo == "the value");
    }

    SECTION("sets string value for valid string switch using a short switch") {
        const char* argv[] = { "program-name", "-fthe value" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 2, argv,
                                        "--foo", "-f", &model.foo);
        CHECK(result == RUN_LOGIC_RESULT);
        CHECK(logicCalled);
        CHECK(model.foo == "the value");
    }

    SECTION("sets string value for valid string switch where parameter is missing") {
        const char* argv[] = { "program-name", "-f" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 2, argv,
                                        "--foo", "-f", &model.foo);
        CHECK(result == 1);
        CHECK_FALSE(logicCalled);
    }

    SECTION("sees parameters as optional") {
        const char* argv[] = { "program-name", "--foo", "the value" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 3, argv,
                                        "--baz", "-b", &model.baz,
                                        "--foo", "-f", &model.foo);
        CHECK(result == RUN_LOGIC_RESULT);
        CHECK(logicCalled);
    }

    SECTION("sees no parameters as OK") {
        const char* argv[] = { "program-name" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 1, argv,
                                        "--foo", "-f", &model.foo);
        CHECK(result == RUN_LOGIC_RESULT);
        CHECK(logicCalled);
    }

    SECTION("can handle multiple parameters") {
        const char* argv[] = { "program-name", "-fthe value", "--baz" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 3, argv,
                                        "--baz", "-b", &model.baz,
                                        "--foo", "-f", &model.foo);
        CHECK(result == RUN_LOGIC_RESULT);
        CHECK(model.baz);
        CHECK(logicCalled);
        CHECK(model.foo == "the value");
    }

    SECTION("sets integer value for valid integer switch for long switch") {
        const char* argv[] = { "program-name", "--bar", "12" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 3, argv,
                                        "--bar", "-b", &model.bar);
        CHECK(result == RUN_LOGIC_RESULT);
        CHECK(model.bar == 12);
        CHECK(logicCalled);
    }

    SECTION("sets integer value for valid integer switch for short switch") {
        const char* argv[] = { "program-name", "-b12" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 2, argv,
                                        "--bar", "-b", &model.bar);
        CHECK(result == RUN_LOGIC_RESULT);
        CHECK(model.bar == 12);
        CHECK(logicCalled);
    }

    SECTION("sets integer value for valid integer switch where parameter is missing") {
        const char* argv[] = { "program-name", "--bar" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 2, argv,
                                        "--bar", "-b", &model.bar);
        CHECK(result == 1);
        CHECK(model.bar == 0);
        CHECK_FALSE(logicCalled);
    }

    SECTION("sets integer value for valid integer switch where parameter is not a string") {
        const char* argv[] = { "program-name", "--bar", "hello" };
        int result = processCommandLine(&showHelpScreen, &runLogic, 3, argv,
                                        "--bar", "-b", &model.bar);
        CHECK(result == 1);
        CHECK(model.bar == 0);
        CHECK_FALSE(logicCalled);
    }
}