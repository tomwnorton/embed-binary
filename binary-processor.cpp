/*
 *  embed-binary: Generate code that represents a binary file as a
 *                constant vector
 *  Copyright (C) 2018 Tom Norton
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <iomanip>
#include <iterator>
#include <algorithm>
#include "binary-processor.hpp"

using namespace std;

// region Custom Stream Iterators
namespace {
    class OutputStreamOutputIterator {
    public:
        typedef void value_type;
        typedef void difference_type;
        typedef void pointer;
        typedef void reference;
        typedef output_iterator_tag iterator_category;

        OutputStreamOutputIterator(ostream& out, unsigned int columnCount);

        // No-op operators
        OutputStreamOutputIterator& operator*() { return *this; }

        OutputStreamOutputIterator& operator++() { return *this; }

        const OutputStreamOutputIterator operator++(int) { return *this; }

        OutputStreamOutputIterator& operator=(char character);

    private:
        bool isFirst() const;

        ostream* m_out;
        unsigned int m_columnCount;
        unsigned int m_index;
    };

    OutputStreamOutputIterator::OutputStreamOutputIterator(ostream& out, unsigned int columnCount)
            : m_out(&out), m_columnCount(columnCount), m_index(0U) {}

    OutputStreamOutputIterator& OutputStreamOutputIterator::operator=(char theByte) {
        auto rawByte = static_cast<uint8_t>(theByte);
        if (isFirst()) {
            *m_out << "\n    ";
        } else if (m_index % m_columnCount == 0) {
            *m_out << ",\n    ";
        } else {
            *m_out << ", ";
        }
        *m_out << "0x" << setw(2) << setfill('0') << hex << uppercase << static_cast<unsigned int>(rawByte);
        ++m_index;
        return *this;
    }

    bool OutputStreamOutputIterator::isFirst() const {
        return m_index == 0U;
    }
}
// endregion

BinaryProcessor::BinaryProcessor(const char* constantName, unsigned int columnCount)
    : m_constantName(constantName), m_columnCount(columnCount) {}

void BinaryProcessor::processBinaryFile(std::istream& in, std::ostream& out) const {
    istreambuf_iterator<char> inputIterator(in);
    istreambuf_iterator<char> inputIteratorEnd;
    OutputStreamOutputIterator outputIterator(out, m_columnCount);
    bool hasElements = inputIterator != inputIteratorEnd;
    out << "const std::vector<std::uint8_t> " << m_constantName << " = {";
    copy(inputIterator, inputIteratorEnd, outputIterator);
    if (hasElements) {
        out << "\n";
    }
    out << "};\n"s;
}