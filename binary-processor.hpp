/*
 *  embed-binary: Generate code that represents a binary file as a
 *                constant vector
 *  Copyright (C) 2018 Tom Norton
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef EMBED_BINARY_BINARY_PROCESSOR_HPP
#define EMBED_BINARY_BINARY_PROCESSOR_HPP

#include <iosfwd>

class BinaryProcessor {
public:
    BinaryProcessor(const char* constantName, unsigned int columnCount);
    BinaryProcessor(const BinaryProcessor&) = delete;
    BinaryProcessor(BinaryProcessor&&) = delete;
    BinaryProcessor& operator=(const BinaryProcessor&) = delete;
    BinaryProcessor& operator=(BinaryProcessor&&) = delete;

    void processBinaryFile(std::istream& in, std::ostream& out) const;
private:
    const char* m_constantName;
    unsigned int m_columnCount;
};

#endif //EMBED_BINARY_BINARY_PROCESSOR_HPP
