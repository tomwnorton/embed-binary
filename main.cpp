/*
 *  embed-binary: Generate code that represents a binary file as a
 *                constant vector
 *  Copyright (C) 2018 Tom Norton
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <fstream>
#include <functional>

#include "command-line-processor.hpp"
#include "binary-processor.hpp"

#ifdef WINDOWS
#include <stdio.h>
#include <fcntl.h>
#include <io.h>

#define CHANGE_STDIN_TO_BINARY_MODE int previousMode = _set_mode(_fileno(stdin), _O_BINARY);\
if (previousMode == -1) {\
    cerr << "Could not set STDIN to binary" << endl;\
    return 1;\
}
#define CHANGE_STDIN_TO_ORIGINAL_MODE _set_mode(_fileno(stdin), previousMode)
#else
#define CHANGE_STDIN_TO_BINARY_MODE
#define CHANGE_STDIN_TO_ORIGINAL_MODE
#endif

using namespace std;

struct Options {
    string constantName = "BINARY_FILE";
    string fileName;
    int columnWidth = 12;
};

void printHelpScreen();
int printFileAsConstantDeclaration(const Options* options);

int main(int argc, char** argv) {
    Options options;
    return processCommandLine(&printHelpScreen, bind(&printFileAsConstantDeclaration, &options),
                              argc, const_cast<const char**>(argv),
                              "--constant-name", "-c", &options.constantName,
                              "--column-width", "-w", &options.columnWidth,
                              "--file-name", "-f", &options.fileName);
}

void printHelpScreen() {
    cerr << "embed-binary [--help | -h] [--constant-name | -c <constant-name>]\n"
         << "             [--column-width | -w <column-width] [--file-name | -f <file-name>]\n"
         << "\n"
         << "--help, -h             Show this help screen\n"
         << "--constant-name, -c    Set the constant name (defaults to BINARY_FILE)\n"
         << "--column-width, -w     Sets the column width (defaults to 12)\n"
         << "--file-name, -f        Sets the file name (defaults to stdin)"
         << endl;
}

int printFileAsConstantDeclaration(const Options* options) {
    if (options->columnWidth <= 0UL) {
        cerr << "column width must be positive" << endl;
        return 1;
    }
    BinaryProcessor binaryProcessor(options->constantName.c_str(), static_cast<unsigned int>(options->columnWidth));
    if (options->fileName.empty()) {
        CHANGE_STDIN_TO_BINARY_MODE;
        binaryProcessor.processBinaryFile(cin, cout);
        CHANGE_STDIN_TO_ORIGINAL_MODE;
        return 0;
    }

    ifstream inFile(options->fileName, ios::in | ios::binary);
    if (inFile.fail()) {
        cerr << "File name must exist" << endl;
        return 1;
    }
    binaryProcessor.processBinaryFile(inFile, cout);
    return 0;
}