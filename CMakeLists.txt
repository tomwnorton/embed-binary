#  embed-binary: Generate code that represents a binary file as a
#                constant vector
#  Copyright (C) 2018 Tom Norton
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
cmake_minimum_required(VERSION 3.10)
project(embed_binary VERSION 1.0.1)

set(CMAKE_CXX_STANDARD 17)

set(SOURCES binary-processor.hpp binary-processor.cpp command-line-processor.hpp)
set(TEST_SOURCES ${SOURCES} binary-processor-test.cpp command-line-processor-test.cpp)

add_executable(embed-binary main.cpp ${SOURCES})
add_executable(embed-binary-tests test-main.cpp ${TEST_SOURCES})
target_include_directories(embed-binary-tests PUBLIC SYSTEM catch)
if (WIN32)
    target_compile_definitions(embed-binary PUBLIC WINDOWS)
endif()

enable_testing()
add_test(NAME the-tests COMMAND embed-binary-tests)

install(TARGETS embed-binary RUNTIME DESTINATION bin)